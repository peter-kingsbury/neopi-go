# NeoPi

## Introduction

## Environment Setup

```bash
sudo apt-get install -y gcc-arm*
```

Compilation steps were taken from [SQLite3](https://github.com/mattn/go-sqlite3/).

## Devices

* Pi Camera V2
* BME280 (Temperature, Pressure, Altitude, Humidity) - internal
* KY-013 (Temperature) - external
* LIS3DH (Accelerometer x,y,z)
* TSL2591 (Luminosity)
* HMC5883L (x,y,z in uT, heading in degrees)
* SI1145 (Visibility, IR, Proximity, UV Level)
* DS3231SN Real-Time Clock
* Ultimate GPS V3
* XBee-PRO 900HP radio transmitter
* Arduino Pro Mini used as a controller for the radio

## References
https://godoc.org/gobot.io/x/gobot/platforms/raspi
