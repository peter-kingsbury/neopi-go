package main

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3" // Import go-sqlite3 library
	"github.com/quhar/bme280"
	"github.com/slayer/autorestart"
	"github.com/yryz/ds18b20"
	"go.bug.st/serial"
	"golang.org/x/exp/io/i2c"
	// periph.io/x/periph/conn/i2c
	"io"
	"log"
	"math/rand"
	"os"
	"sync"
	"time"
	xbee "neopi/src/xbee"
)

var deviceBus = "/dev/i2c-1"
var databaseFilename = "data.db"

type Device int
const (
	NoDevice Device = iota
	BME280   			// 0x77 Temperature, Pressure, Altitude, Humidity (internal)
	DS18B20				// 1-w  Analog temperature
	HMC5883L			// 0x1e Compass x,y,z in uT, heading in degrees
	LIS3DH				// 0x18 Accelerometer x,y,z
	MPL3115A2			// 0x60 pressure, temperature, altitude
	SI1145				// 0x60 Visibility, IR, Proximity, UV Level
	TSL2591				// 0x29 Luminosity
	GPS					// 0x?? Ultimate GPS V3
	XBeePro900HP		// 0x?? XBee-PRO 900HP radio transmitter
	ArduinoProMini		// 0x04 Arduino Pro Mini used as a controller for the radio
	Camera				// 0x?? Pi Camera V2
	Armature			// SG90 armature

	RandomNumberGenerator
)

type Property int
const (
	NoProperty Property = iota
	Temperature
	Pressure
	Humidity
	X
	Y
	Z
	Heading
	Visibility
	IR
	Proximity
	UV
	Luminosity
	Latitude
	Longitude
	Speed
	Altitude
	Angle
)

type Reading struct {
	Device Device
	Property Property
	Value float64
	Timestamp int64 //  time.Now().UnixNano(), number of nano seconds passed since Unix epoch
}

func readRegister(d *i2c.Device, reg byte) byte {
	buf := make([]byte, 1)
	err := d.ReadReg(reg, buf)
	if err != nil { log.Fatal(err) }
	return buf[0]
}

func readRegisterWord(d *i2c.Device, reg byte) uint16 {
	return uint16(readRegister(d, reg + 1)) << 8 | uint16(readRegister(d, reg))
}

//func write(d *i2c.Device, buf []byte) {
//	err := d.Write(buf)
//	if err != nil {
//		log.Fatal(err)
//	}
//}

func writeRegister(d *i2c.Device, reg byte, buf []byte) {
	err := d.WriteReg(reg, buf)
	if err != nil {
		log.Fatal(err)
	}
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

func main() {
	var wg sync.WaitGroup

	// Initialize logger
	logfile := startLogger()
	defer logfile.Close()

	// Initialize auto-restart
	startAutoRestarter()

	// Initialize database
	db := startDatabase(databaseFilename)
	defer db.Close()

	radio := startRadio()
	defer radio.Disconnect()

	//return

	// Initialize RNG
	//wg.Add(1)
	//go startRNG(&wg, db)

	// Initialize BME280
	wg.Add(1)
	go startBME280(&wg, db)

	// Initialize LIS3DH
	wg.Add(1)
	go startLIS3DH(&wg, db)

	// Initialize DS18B20
	wg.Add(1)
	go startDS18B20(&wg, db)

	// Initialize HMC5883L
	wg.Add(1)
	go startHMC5883L(&wg, db)

	// Initialize SI1145
	wg.Add(1)
	go startSI1145(&wg, db)

	// Wait until all device-loops exit
	wg.Wait()
}

func startRadio() *xbee.XBee {
	ports, err := serial.GetPortsList()
	if err != nil {
		log.Fatal(err)
	}
	if len(ports) == 0 {
		log.Fatal("No serial ports found!")
	}
	for _, port := range ports {
		fmt.Printf("Found port: %v\n", port)
	}

	var cfg = map[string]xbee.Config{
		"Payload": {
			//Device:      "/dev/serial0",
			Device:      "/dev/ttyAMA0",
			//Device:      "/dev/ttyS0",
			BaudRate:    9600,
			Destination: "0013A200415B3C95",
			Programs:    map[string]xbee.Program{
				"boot":{
					Name: "boot",
					Commands: []xbee.Command{
						{ Cmd: "ATRE", Arg: "" },
						{ Cmd: "ATNI", Arg: "Green" },
						{ Cmd: "ATHP", Arg: "0" },
						{ Cmd: "ATID", Arg: "7FFF" },
						{ Cmd: "ATCE", Arg: "2" },
						{ Cmd: "ATBD", Arg: "7" },
						{ Cmd: "ATAP", Arg: "1" },
						{ Cmd: "ATAO", Arg: "0" },
						{ Cmd: "ATDH", Arg: "13A200" },
						{ Cmd: "ATDL", Arg: "415B3C95" },
						{ Cmd: "ATWR", Arg: "" },
					},
				},
			},
		},
		"MissionControl": {
			Device:      "/dev/ttyUSB0",
			BaudRate:    115200,
			Destination: "0013A200415B3CA7",
			Programs:    map[string]xbee.Program{
				"boot":{
					Name: "boot",
					Commands: []xbee.Command{
						{ Cmd: "ATRE", Arg: "" },
						{ Cmd: "ATNI", Arg: "Blue" },
						{ Cmd: "ATHP", Arg: "0" },
						{ Cmd: "ATID", Arg: "7FFF" },
						{ Cmd: "ATCE", Arg: "1" },
						{ Cmd: "ATBD", Arg: "7" },
						{ Cmd: "ATAP", Arg: "1" },
						{ Cmd: "ATAO", Arg: "0" },
						{ Cmd: "ATDH", Arg: "13A200" },
						{ Cmd: "ATDL", Arg: "415B3CA7" },
						{ Cmd: "ATWR", Arg: "" },
					},
				},
			},
		},
	}

	radio := xbee.New(cfg["Payload"])
	radio.Connect()
	radio.RunNamedProgram("boot")

	return radio
}

func startAutoRestarter() {
	autorestart.StartWatcher()
}

func startRNG(wg *sync.WaitGroup, db *sql.DB) {
	for {
		reading := Reading{
			Device: RandomNumberGenerator,
			Property: NoProperty,
			Value: 0.0 + rand.Float64() * (1.0 - 0.0),
			Timestamp: getTimestamp(),
		}

		log.Printf("%v", reading)

		storeReading(db, &reading)

		time.Sleep(1000 * time.Millisecond)
	}
}

func startLogger() *os.File {
	logfile, err := os.OpenFile("neopi.log", os.O_CREATE | os.O_APPEND | os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}

	mw := io.MultiWriter(os.Stdout, logfile)
	log.SetOutput(mw)
	log.Printf("Starting up NeoPi")
	return logfile
}

func startDatabase(filename string) *sql.DB {
	//_ = os.Remove(filename) // I delete the file to avoid duplicated records. SQLite is a file based database.
	log.Printf("Creating %s ...", filename)
	file, err := os.Create(filename) // Create SQLite file
	if err != nil {
		log.Fatal(err.Error())
	}
	_ = file.Close()
	log.Printf("%s created", filename)

	db, _ := sql.Open("sqlite3", filename) // Open the created SQLite File

	createDatabaseTables(db)
	createDatabaseSeedData(db)

	return db
}

func createDatabaseTables(db *sql.DB) {
	// create table if not exists
	rawSql := `
	CREATE TABLE IF NOT EXISTS readings (
		Id INTEGER PRIMARY KEY AUTOINCREMENT,
		Device INTEGER,
		Property INTEGER,
		Value FLOAT,
		CreatedAt DATETIME
	);
	`
	_, err := db.Exec(rawSql)
	if err != nil { log.Fatal(err) }

	rawSql = `
	CREATE TABLE IF NOT EXISTS devices (
		Id INTEGER PRIMARY KEY AUTOINCREMENT,
		Name VARCHAR(255),
	    Description VARCHAR(2048)
	);
	`
	_, err = db.Exec(rawSql)
	if err != nil { log.Fatal(err) }

	rawSql = `
	CREATE TABLE IF NOT EXISTS properties (
		Id INTEGER PRIMARY KEY AUTOINCREMENT,
		Name VARCHAR(255)
	);
	`
	_, err = db.Exec(rawSql)
	if err != nil { log.Fatal(err) }
}

func createDatabaseSeedData(db *sql.DB) {
	upsertProperty(db, Temperature, "Temperature")
	upsertProperty(db, Pressure   , "Pressure")
	upsertProperty(db, Humidity,    "Humidity")
	upsertProperty(db, X,           "X")
	upsertProperty(db, Y,           "Y")
	upsertProperty(db, Z,           "Z")
	upsertProperty(db, Heading,     "Heading")
	upsertProperty(db, Visibility,  "Visibility")
	upsertProperty(db, IR,          "IR")
	upsertProperty(db, Proximity,   "Proximity")
	upsertProperty(db, UV,          "UV")
	upsertProperty(db, Luminosity,  "Luminosity")
	upsertProperty(db, Latitude,    "Latitude")
	upsertProperty(db, Longitude,   "Longitude")
	upsertProperty(db, Speed,       "Speed")
	upsertProperty(db, Altitude,    "Altitude")
	upsertProperty(db, Angle,       "Angle")

	upsertDevice(db, BME280,         "BME280",         "Temperature, pressure, humidity")
	upsertDevice(db, DS18B20,        "DS18B20",        "Analog 1-wire temperature sensor")
	upsertDevice(db, HMC5883L,       "HMC5883L",       "Compass")
	upsertDevice(db, LIS3DH,         "LIS3DH",         "Accelerometer")
	upsertDevice(db, MPL3115A2,      "MPL3115A2",      "Pressure, temperature, altitude")
	upsertDevice(db, SI1145,         "SI1145",         "Visibility, IR, Proximity, UV Level")
	upsertDevice(db, TSL2591,        "TSL2591",        "Luminosity")
	upsertDevice(db, GPS,            "GPS",            "GPS receiver")
	upsertDevice(db, XBeePro900HP,   "XBeePro900HP",   "XBee Pro 900 HP radio transceiver")
	upsertDevice(db, ArduinoProMini, "ArduinoProMini", "Arduino Pro Mini")
	upsertDevice(db, Camera,         "Camera",         "Raspi Camera V2")
	upsertDevice(db, Armature,       "SG90",           "Armature")
}

func getTimestamp() int64 {
	return time.Now().UnixNano()
}

func upsertDevice(db *sql.DB, id Device, name string, description string) {
	rawSql := `
	INSERT INTO devices (id, name, description)
	  VALUES(?, ?, ?) 
	  ON CONFLICT(id) 
	  DO UPDATE SET name=excluded.name, description=excluded.description;
	`
	stmt, err := db.Prepare(rawSql)
	if err != nil { log.Fatal(err) }
	defer stmt.Close()

	_, err2 := stmt.Exec(id, name, description)
	if err2 != nil { log.Fatal(err2) }
}

func upsertProperty(db *sql.DB, id Property, name string) {
	rawSql := `
	INSERT INTO properties (id, name)
	  VALUES(?, ?) 
	  ON CONFLICT(id) 
	  DO UPDATE SET name=excluded.name;
	`
	stmt, err := db.Prepare(rawSql)
	if err != nil { log.Fatal(err) }
	defer stmt.Close()

	_, err2 := stmt.Exec(id, name)
	if err2 != nil { log.Fatal(err2) }
}

func storeReadings(db *sql.DB, readings []Reading) {
	rawSql := `
	INSERT OR REPLACE INTO readings (
		Device,
		Property,
		Value,
		CreatedAt
	) values(?, ?, ?, ?)
	`

	stmt, err := db.Prepare(rawSql)
	if err != nil { log.Fatal(err) }
	defer stmt.Close()

	for _, reading := range readings {
		_, err2 := stmt.Exec(
			reading.Device,
			reading.Property,
			reading.Value,
			reading.Timestamp)
		if err2 != nil { log.Fatal(err2) }
	}
}

func storeReading(db *sql.DB, reading *Reading) {
	rawSql := `
	INSERT OR REPLACE INTO readings (
		Device,
		Property,
		Value,
		CreatedAt
	) values(?, ?, ?, ?)
	`

	stmt, err := db.Prepare(rawSql)
	if err != nil { log.Fatal(err) }
	defer stmt.Close()

	_, err2 := stmt.Exec(
		reading.Device,
		reading.Property,
		reading.Value,
		reading.Timestamp)
	if err2 != nil { log.Fatal(err2) }
}

func startBME280(wg *sync.WaitGroup, db *sql.DB) {
	defer wg.Done()

	// Load BME280
	d, err := i2c.Open(&i2c.Devfs{Dev: deviceBus}, bme280.I2CAddr)
	if err != nil { log.Fatal(err.Error()) }
	defer d.Close()

	b := bme280.New(d)
	err = b.Init()

	readings := make([]Reading, 3)

	readings[0].Device = BME280
	readings[0].Property = Temperature

	readings[1].Device = BME280
	readings[1].Property = Pressure

	readings[2].Device = BME280
	readings[2].Property = Humidity

	for {
		t, p, h, err := b.EnvData()
		if err != nil { log.Fatal(err.Error()) }

		log.Printf("[BME280] Temperature: %.2f°C, Press: %.2fhPa, Hum: %.2f%%", t, p, h)

		now := getTimestamp()

		// Temperature
		readings[0].Value = t
		readings[0].Timestamp = now

		// Pressure
		readings[1].Value = p
		readings[1].Timestamp = now

		// Humidity
		readings[2].Value = h
		readings[2].Timestamp = now

		storeReadings(db, readings)

		time.Sleep(1000 * time.Millisecond)
	}
}

func startDS18B20(wg *sync.WaitGroup, db *sql.DB) {
	defer wg.Done()

	sensors, err := ds18b20.Sensors()
	if err != nil { log.Fatal(err) }

	reading := Reading{
		Device:    DS18B20,
		Property:  Temperature,
	}

	for {
		for _, sensor := range sensors {
			t, err := ds18b20.Temperature(sensor)
			if err != nil { log.Fatal(err) }
			log.Printf("[DS18B20] Temperature: %.2f°C", t)

			reading.Value = t
			reading.Timestamp = getTimestamp()

			storeReading(db, &reading)

			time.Sleep(1000 * time.Millisecond)
		}
	}
}

func startHMC5883L(wg *sync.WaitGroup, db *sql.DB) {
	defer wg.Done()

	d, err := i2c.Open(&i2c.Devfs{Dev: deviceBus}, 0x1e)
	if err != nil { log.Fatal(err) }
	defer d.Close()

	const REG = 0
	const ConfigARegister = 0x00
	const ScaleRegister = 0x01
	const ModeRegister = 0x02
	const DefaultCfgAMa = 0x03
	const DefaultSampleRate = 15
	const DefaultScalar = float64(0.73)
	const ReadBlock = 0x00
	const ModeMeasureContinuous = 0x00

	configAValue := byte((DefaultCfgAMa << 5) | (DefaultSampleRate << 2))
	scaleValue := byte(REG << 5)

	writeRegister(d, ConfigARegister, []byte{configAValue})
	writeRegister(d, ScaleRegister, []byte{scaleValue})
	writeRegister(d, ModeRegister, []byte{ModeMeasureContinuous})

	twosCompliment := func(val int, len int) int {
		num := 0
		if val & (1 << len - 1) != 0 {
			num = val - (1<<len)
		}
		return num
	}

	convert := func(data []byte, offset int) float64 {
		num := twosCompliment(int(data[offset]) << 8 | int(data[offset+1]), 16)
		return DefaultScalar * float64(num)
	}

	readings := make([]Reading, 3)

	readings[0].Device = HMC5883L
	readings[0].Property = X

	readings[1].Device = HMC5883L
	readings[1].Property = Y

	readings[2].Device = HMC5883L
	readings[2].Property = Z

	buf := make([]byte, 12)

	for {
		// Read from the sensor
		err := d.ReadReg(ReadBlock, buf)
		if err != nil { log.Fatal(err) }

		// Convert data
		x := convert(buf, 3)
		y := convert(buf, 7)
		z := convert(buf, 5)

		// Output to log
		log.Printf("[HMC5883L] x = %.1f, y = %.1f, z = %.1f", x, y, z)

		// Store in database
		now := getTimestamp()

		// X
		readings[0].Value = x
		readings[0].Timestamp = now

		// Y
		readings[1].Value = y
		readings[1].Timestamp = now

		// Z
		readings[2].Value = z
		readings[2].Timestamp = now

		storeReadings(db, readings)

		// Sleep until next cycle
		time.Sleep(1000 * time.Millisecond)
	}
}

func startLIS3DH(wg *sync.WaitGroup, db *sql.DB) {
	defer wg.Done()

	d, err := i2c.Open(&i2c.Devfs{Dev: deviceBus}, 0x18)
	if err != nil { log.Fatal(err) }
	defer d.Close()

	const ControlRegister1 = 0x20
	const ControlRegister4 = 0x23
	const OutXL = 0x28
	const OutXH = 0x29
	const OutYL = 0x2a
	const OutYH = 0x2b
	const OutZL = 0x2c
	const OutZH = 0x2d

	// Enable the device
	writeRegister(d, ControlRegister4, []byte{0x7f})

	// Set high resolution
	writeRegister(d, ControlRegister1, []byte{0x08})

	readings := make([]Reading, 3)

	readings[0].Device = LIS3DH
	readings[0].Property = X

	readings[1].Device = LIS3DH
	readings[1].Property = Y

	readings[2].Device = LIS3DH
	readings[2].Property = Z

	for {
		x := float64((int(readRegister(d, OutXH)) << 8 | int(readRegister(d, OutXL))) >> 4) / 1024
		y := float64((int(readRegister(d, OutYH)) << 8 | int(readRegister(d, OutYL))) >> 4) / 1024
		z := float64((int(readRegister(d, OutZH)) << 8 | int(readRegister(d, OutZL))) >> 4) / 1024

		log.Printf("[LIS3DH] x = %.1f, y = %.1f, z = %.1f", x, y, z)

		// Store in database
		now := getTimestamp()

		// X
		readings[0].Value = x
		readings[0].Timestamp = now

		// Y
		readings[1].Value = y
		readings[1].Timestamp = now

		// Z
		readings[2].Value = z
		readings[2].Timestamp = now

		storeReadings(db, readings)

		// TODO: Add reading logic
		time.Sleep(1000 * time.Millisecond)
	}
}

func startSI1145(wg *sync.WaitGroup, db *sql.DB) {
	defer wg.Done()

	// COMMANDS
	const SI1145_PARAM_QUERY                      = 0x80
	const SI1145_PARAM_SET                        = 0xA0
	const SI1145_NOP                              = 0x0
	const SI1145_RESET                            = 0x01
	const SI1145_BUSADDR                          = 0x02
	const SI1145_PS_FORCE                         = 0x05
	const SI1145_ALS_FORCE                        = 0x06
	const SI1145_PSALS_FORCE                      = 0x07
	const SI1145_PS_PAUSE                         = 0x09
	const SI1145_ALS_PAUSE                        = 0x0A
	const SI1145_PSALS_PAUSE                      = 0xB
	const SI1145_PS_AUTO                          = 0x0D
	const SI1145_ALS_AUTO                         = 0x0E
	const SI1145_PSALS_AUTO                       = 0x0F
	const SI1145_GET_CAL                          = 0x12

	// Parameters
	const SI1145_PARAM_I2CADDR                    = 0x00
	const SI1145_PARAM_CHLIST                     = 0x01
	const SI1145_PARAM_CHLIST_ENUV                = 0x80
	const SI1145_PARAM_CHLIST_ENAUX               = 0x40
	const SI1145_PARAM_CHLIST_ENALSIR             = 0x20
	const SI1145_PARAM_CHLIST_ENALSVIS            = 0x10
	const SI1145_PARAM_CHLIST_ENPS1               = 0x01
	const SI1145_PARAM_CHLIST_ENPS2               = 0x02
	const SI1145_PARAM_CHLIST_ENPS3               = 0x04

	const SI1145_PARAM_PSLED12SEL                 = 0x02
	const SI1145_PARAM_PSLED12SEL_PS2NONE         = 0x00
	const SI1145_PARAM_PSLED12SEL_PS2LED1         = 0x10
	const SI1145_PARAM_PSLED12SEL_PS2LED2         = 0x20
	const SI1145_PARAM_PSLED12SEL_PS2LED3         = 0x40
	const SI1145_PARAM_PSLED12SEL_PS1NONE         = 0x00
	const SI1145_PARAM_PSLED12SEL_PS1LED1         = 0x01
	const SI1145_PARAM_PSLED12SEL_PS1LED2         = 0x02
	const SI1145_PARAM_PSLED12SEL_PS1LED3         = 0x04

	const SI1145_PARAM_PSLED3SEL                  = 0x03
	const SI1145_PARAM_PSENCODE                   = 0x05
	const SI1145_PARAM_ALSENCODE                  = 0x06

	const SI1145_PARAM_PS1ADCMUX                  = 0x07
	const SI1145_PARAM_PS2ADCMUX                  = 0x08
	const SI1145_PARAM_PS3ADCMUX                  = 0x09
	const SI1145_PARAM_PSADCOUNTER                = 0x0A
	const SI1145_PARAM_PSADCGAIN                  = 0x0B
	const SI1145_PARAM_PSADCMISC                  = 0x0C
	const SI1145_PARAM_PSADCMISC_RANGE            = 0x20
	const SI1145_PARAM_PSADCMISC_PSMODE           = 0x04

	const SI1145_PARAM_ALSIRADCMUX                = 0x0E
	const SI1145_PARAM_AUXADCMUX                  = 0x0F

	const SI1145_PARAM_ALSVISADCOUNTER            = 0x10
	const SI1145_PARAM_ALSVISADCGAIN              = 0x11
	const SI1145_PARAM_ALSVISADCMISC              = 0x12
	const SI1145_PARAM_ALSVISADCMISC_VISRANGE     = 0x20

	const SI1145_PARAM_ALSIRADCOUNTER             = 0x1D
	const SI1145_PARAM_ALSIRADCGAIN               = 0x1E
	const SI1145_PARAM_ALSIRADCMISC               = 0x1F
	const SI1145_PARAM_ALSIRADCMISC_RANGE         = 0x20

	const SI1145_PARAM_ADCCOUNTER_511CLK          = 0x70

	const SI1145_PARAM_ADCMUX_SMALLIR             = 0x00
	const SI1145_PARAM_ADCMUX_LARGEIR             = 0x03

	// REGISTERS
	const SI1145_REG_PARTID                       = 0x00
	const SI1145_REG_REVID                        = 0x01
	const SI1145_REG_SEQID                        = 0x02

	const SI1145_REG_INTCFG                       = 0x03
	const SI1145_REG_INTCFG_INTOE                 = 0x01
	const SI1145_REG_INTCFG_INTMODE               = 0x02

	const SI1145_REG_IRQEN                        = 0x04
	const SI1145_REG_IRQEN_ALSEVERYSAMPLE         = 0x01
	const SI1145_REG_IRQEN_PS1EVERYSAMPLE         = 0x04
	const SI1145_REG_IRQEN_PS2EVERYSAMPLE         = 0x08
	const SI1145_REG_IRQEN_PS3EVERYSAMPLE         = 0x10

	const SI1145_REG_IRQMODE1                     = 0x05
	const SI1145_REG_IRQMODE2                     = 0x06

	const SI1145_REG_HWKEY                        = 0x07
	const SI1145_REG_MEASRATE0                    = 0x08
	const SI1145_REG_MEASRATE1                    = 0x09
	const SI1145_REG_PSRATE                       = 0x0A
	const SI1145_REG_PSLED21                      = 0x0F
	const SI1145_REG_PSLED3                       = 0x10
	const SI1145_REG_UCOEFF0                      = 0x13
	const SI1145_REG_UCOEFF1                      = 0x14
	const SI1145_REG_UCOEFF2                      = 0x15
	const SI1145_REG_UCOEFF3                      = 0x16
	const SI1145_REG_PARAMWR                      = 0x17
	const SI1145_REG_COMMAND                      = 0x18
	const SI1145_REG_RESPONSE                     = 0x20
	const SI1145_REG_IRQSTAT                      = 0x21
	const SI1145_REG_IRQSTAT_ALS                  = 0x01

	const SI1145_REG_ALSVISDATA0                  = 0x22
	const SI1145_REG_ALSIRDATA0                   = 0x24
	const SI1145_REG_ALSIRDATA1                   = 0x25
	const SI1145_REG_PS1DATA0                     = 0x26
	const SI1145_REG_PS1DATA1                     = 0x27
	const SI1145_REG_PS2DATA0                     = 0x28
	const SI1145_REG_PS2DATA1                     = 0x29
	const SI1145_REG_PS3DATA0                     = 0x2A
	const SI1145_REG_PS3DATA1                     = 0x2B
	const SI1145_REG_UVINDEX0                     = 0x2C
	const SI1145_REG_UVINDEX1                     = 0x2D
	const SI1145_REG_PARAMRD                      = 0x2E
	const SI1145_REG_CHIPSTAT                     = 0x30

	// I2C Address
	const SI1145_ADDR                             = 0x60


	d, err := i2c.Open(&i2c.Devfs{Dev: deviceBus}, SI1145_ADDR)
	if err != nil { log.Fatal(err) }
	defer d.Close()

	reset := func() {
		writeRegister(d, SI1145_REG_MEASRATE0, []byte{0})
		writeRegister(d, SI1145_REG_MEASRATE1, []byte{0})
		writeRegister(d, SI1145_REG_IRQEN, []byte{0})
		writeRegister(d, SI1145_REG_IRQMODE1, []byte{0})
		writeRegister(d, SI1145_REG_IRQMODE2, []byte{0})
		writeRegister(d, SI1145_REG_INTCFG, []byte{0})
		writeRegister(d, SI1145_REG_IRQSTAT, []byte{0xff})
		writeRegister(d, SI1145_REG_COMMAND, []byte{SI1145_RESET})
	}

	writeParam := func(p byte, v byte) byte {
		writeRegister(d, SI1145_REG_PARAMWR, []byte{v})
		writeRegister(d, SI1145_REG_COMMAND, []byte{p | SI1145_PARAM_SET})
		return readRegister(d, SI1145_REG_PARAMRD)
	}

	init := func() {
		// Enable UVindex measurement coefficients
		writeRegister(d, SI1145_REG_UCOEFF0, []byte{0x29})
		writeRegister(d, SI1145_REG_UCOEFF1, []byte{0x89})
		writeRegister(d, SI1145_REG_UCOEFF2, []byte{0x02})
		writeRegister(d, SI1145_REG_UCOEFF3, []byte{0x00})
		// Enable UV sensor
		writeParam(SI1145_PARAM_CHLIST, SI1145_PARAM_CHLIST_ENUV | SI1145_PARAM_CHLIST_ENALSIR | SI1145_PARAM_CHLIST_ENALSVIS | SI1145_PARAM_CHLIST_ENPS1)
		// Enable interrupt on every sample
		writeRegister(d, SI1145_REG_INTCFG, []byte{SI1145_REG_INTCFG_INTOE})
		writeRegister(d, SI1145_REG_IRQEN, []byte{SI1145_REG_IRQEN_ALSEVERYSAMPLE})
		// Program LED current
		writeRegister(d, SI1145_REG_PSLED21, []byte{0x03})
		writeParam(SI1145_PARAM_PS1ADCMUX, SI1145_PARAM_ADCMUX_LARGEIR)
		// Prox sensor #1 uses LED #1
		writeParam(SI1145_PARAM_PSLED12SEL, SI1145_PARAM_PSLED12SEL_PS1LED1)
		// Fastest clocks, clock div 1
		writeParam(SI1145_PARAM_PSADCGAIN, 0)
		// Take 511 clocks to measure
		writeParam(SI1145_PARAM_PSADCOUNTER, SI1145_PARAM_ADCCOUNTER_511CLK)
		// in prox mode, high range
		writeParam(SI1145_PARAM_PSADCMISC, SI1145_PARAM_PSADCMISC_RANGE | SI1145_PARAM_PSADCMISC_PSMODE)
		writeParam(SI1145_PARAM_ALSIRADCMUX, SI1145_PARAM_ADCMUX_SMALLIR)
		// Fastest clocks, clock div 1
		writeParam(SI1145_PARAM_ALSIRADCGAIN, 0)
		// Take 511 clocks to measure
		writeParam(SI1145_PARAM_ALSIRADCOUNTER, SI1145_PARAM_ADCCOUNTER_511CLK)
		// in high range mode
		writeParam(SI1145_PARAM_ALSIRADCMISC, SI1145_PARAM_ALSIRADCMISC_RANGE)
		// fastest clocks, clock div 1
		writeParam(SI1145_PARAM_ALSVISADCGAIN, 0)
		// Take 511 clocks to measure
		writeParam(SI1145_PARAM_ALSVISADCOUNTER, SI1145_PARAM_ADCCOUNTER_511CLK)
		// in high range mode (not normal signal)
		writeParam(SI1145_PARAM_ALSVISADCMISC, SI1145_PARAM_ALSVISADCMISC_VISRANGE)
		// measurement rate for auto
		writeRegister(d, SI1145_REG_MEASRATE0, []byte{0xFF}) // 255 * 31.25uS = 8ms
		// auto run
		writeRegister(d, SI1145_REG_COMMAND, []byte{SI1145_PSALS_AUTO})
	}

	reset()
	init()

	readings := make([]Reading, 4)

	readings[0].Device = SI1145
	readings[0].Property = UV

	readings[1].Device = SI1145
	readings[1].Property = Visibility

	readings[2].Device = SI1145
	readings[2].Property = IR

	readings[3].Device = SI1145
	readings[3].Property = Proximity

	for {
		uv   := readRegisterWord(d, SI1145_REG_UVINDEX0)
		vis  := readRegisterWord(d, SI1145_REG_ALSVISDATA0)
		ir   := readRegisterWord(d, SI1145_REG_ALSIRDATA0)
		prox := readRegisterWord(d, SI1145_REG_PS1DATA0)

		log.Printf("[SI1145] UV: %d, Vis: %d, IR: %d, Prox: %d", uv, vis, ir, prox)

		// Store in database
		now := getTimestamp()

		// UV
		readings[0].Value = float64(uv)
		readings[0].Timestamp = now

		// Visibility
		readings[1].Value = float64(vis)
		readings[1].Timestamp = now

		// IR
		readings[2].Value = float64(ir)
		readings[2].Timestamp = now

		// Proximity
		readings[3].Value = float64(prox)
		readings[3].Timestamp = now

		storeReadings(db, readings)

		time.Sleep(1000 * time.Millisecond)
	}
}

func startTSL2591(wg *sync.WaitGroup, db *sql.DB) {
	defer wg.Done()

	// TODO: Add init logic

	for {
		// TODO: Add reading logic
		time.Sleep(1000 * time.Millisecond)
	}
}

func startGPS(wg *sync.WaitGroup, db *sql.DB) {
	defer wg.Done()

	// TODO: Add init logic

	for {
		// TODO: Add reading logic
		time.Sleep(1000 * time.Millisecond)
	}
}

func startXBeePro900HP(wg *sync.WaitGroup, db *sql.DB) {
	defer wg.Done()

	// TODO: Add init logic

	for {
		// TODO: Add reading logic
		time.Sleep(1000 * time.Millisecond)
	}
}

func startArmature(wg *sync.WaitGroup, db *sql.DB) {

}
