module neopi

go 1.15

require (
	github.com/binzume/avr-lis3dh v0.0.0-20160312143043-791dcf9e7bfb // indirect
	github.com/binzume/gob3m v0.0.0-20160312135208-40b486b8a90a // indirect
	github.com/davidgs/bme280_go v0.0.0-20201013184139-35c86ffbf693
	github.com/mattn/go-sqlite3 v1.14.5
	github.com/quhar/bme280 v0.1.0
	github.com/slayer/autorestart v0.0.0-20170706172704-7bc8d250279b
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07 // indirect
	github.com/yryz/ds18b20 v0.0.0-20200527154408-4a8f84bb82d4
	go.bug.st/serial v1.1.1
	golang.org/x/exp v0.0.0-20201008143054-e3b2a7f2fdc7
)
