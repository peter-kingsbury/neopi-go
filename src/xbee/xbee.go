package xbee

import (
	"errors"
	"fmt"
	"go.bug.st/serial"
	"log"
	"strings"
	"time"
)

const XBEE_COMMAND_DELAY = 250
const XBEE_RECV_BUFFER_SIZE = 100

type Command struct {
	Cmd string
	Arg string
}

type Program struct {
	Name string
	Commands []Command
}

type Config struct {
	Device      string
	BaudRate    int
	Destination string
	Programs    map[string]Program
}

type XBee struct {
	device string
	baudRate int
	port serial.Port
	config Config
}

func New(config Config) *XBee {
	x := XBee{
		device: config.Device,
		baudRate: config.BaudRate,
		port:     nil,
	}

	x.config = config

	return &x
}

func (x *XBee) Connect() {
	mode := &serial.Mode{
		BaudRate: x.baudRate,
	}

	log.Printf("Opening serial port %s", x.device)
	port, err := serial.Open(x.device, mode)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Serial port %s opened", x.device)

	x.Delay(XBEE_COMMAND_DELAY)

	x.port = port
}

func (x *XBee) Disconnect() error {
	return x.port.Close()
}

func (x *XBee) EnterCommandMode() {
	log.Printf("Entering command mode")
	x.Command("+++", "")
}

func (x *XBee) ExitCommandMode() {
	log.Printf("Exiting command mode")
	x.Command("ATCN", "")
}

func (x *XBee) RunNamedProgram(name string) {
	x.RunProgram(x.config.Programs[name])
}

func (x *XBee) RunProgram(program Program) {
	log.Printf("Running radio program %s", program.Name)
	x.EnterCommandMode()
	for _, command := range program.Commands {
		x.Command(command.Cmd, command.Arg)
	}
	x.ExitCommandMode()
}

func (x *XBee) Command(cmd string, arg string) {
	verifyResponse := arg
	cmdMode := false

	if cmd == "+++" {
		cmdMode = true
		verifyResponse = "OK"
	}

	if cmdMode == true {
		x.Delay(1000)
	}

	// TODO: Pause the port?

	log.Printf("Writing to serial device")
	if cmdMode == true {
		x.WriteRaw(cmd, false)
	} else {
		if arg == "" {
			x.WriteRaw(cmd, true)
		} else {
			x.WriteRaw(fmt.Sprintf("%s %s", cmd, arg), true)
		}
	}

	x.Delay(XBEE_COMMAND_DELAY)

	log.Printf("Reading from serial device")

	// Verify the response
	buffer := x.ReadRaw(XBEE_RECV_BUFFER_SIZE)
	buffer = strings.Trim(buffer, "\r\n ")

	if cmd == "ATCN" || cmd == "ATRE" || cmd == "ATWR" {
		log.Printf("%s %s -> %s", cmd, arg, buffer)
		return
	}

	log.Printf("Verifying command")

	err := x.VerifyCommand(cmd, verifyResponse)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("%s %s -> %s (verified)", cmd, arg, buffer)
}

func (x *XBee) VerifyCommand(cmd string, expected string) error {
	if cmd == "+++" {
		x.WriteRaw("AT", true)
	} else {
		x.WriteRaw(cmd, true)
	}

	x.Delay(XBEE_COMMAND_DELAY)

	buffer := x.ReadRaw(XBEE_RECV_BUFFER_SIZE)
	buffer = strings.Trim(buffer, "\r\n ")

	// log.Printf("cmd=%s expected=%s response=%s", cmd, expected, buffer)

	if buffer != expected {
		return errors.New(fmt.Sprintf("Expected '%s' but got '%s'", cmd, buffer))
	}

	return nil
}

func (x *XBee) WriteRaw(text string, crlf bool) int {
	out := text
	if crlf == true {
		out = fmt.Sprintf("%s\r", text)
	}

	n, err := x.port.Write([]byte(out))
	if err != nil { log.Fatal(err) }

	return n
}

func (x *XBee) ReadRaw(size int) string {
	buf := make([]byte, size)
	n, err := x.port.Read(buf)
	if err != nil { log.Fatal(err) }
	return string(buf[:n])
}

func (x *XBee) Delay(ms int) {
	time.Sleep(time.Millisecond * time.Duration(ms))
}
