#!/bin/bash

DBNAME=data.db
REMOTEHOST=pi@10.0.1.124
REMOTEDIR=/home/pi/neopi
REMOTEDB="${REMOTEDIR}/${DBNAME}"

rm -rf ./${DBNAME}
scp "${REMOTEHOST}:${REMOTEDB}" .
