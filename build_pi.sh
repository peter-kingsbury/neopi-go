#!/bin/bash

OS=linux
ARCH=arm
BUILD_DIR=./bin
BIN=neopi.arm
OUT_DIR=${BUILD_DIR}/${BIN}

mkdir -p ${BUILD_DIR}
env CC=arm-linux-gnueabi-gcc \
    CXX=arm-linux-gnueabihf-g++ \
    CGO_ENABLED=1 \
    GOOS=${OS} \
    GOARCH=${ARCH} \
    go build -v -o ${OUT_DIR}

REMOTE_HOST=pi@10.0.1.124
REMOTE_DIR=/home/pi/neopi

ssh ${REMOTE_HOST} pkill -9 ${BIN}
scp ${OUT_DIR} ${REMOTE_HOST}:${REMOTE_DIR}
#ssh -f ${REMOTE_HOST} ${REMOTE_DIR}/${BIN} < /dev/null > std.out 2> std.err &
